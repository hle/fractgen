fractgen (2.1.5-1) unstable; urgency=medium

  * New upstream release.
  * Migrate to debhelper-compat (delete debian/compat).
  * debian/control:
    - Bump Standards-Version to 4.4.0.
    - Bump debhelper dependency to = 12.

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 26 Jul 2019 15:30:08 -0300

fractgen (2.1.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 4.3.0.
    - Add new cmake build dependency.
  * Use manpage from src/ and remove debian/ one.
  * Stop installing examples manually, new build system cmake is
    handling it automatically.
  * Update copyright years.

 -- Hugo Lefeuvre <hle@debian.org>  Sat, 02 Feb 2019 14:46:46 +0100

fractgen (2.1.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 4.1.5.

 -- Hugo Lefeuvre <hle@debian.org>  Sat, 21 Jul 2018 09:36:50 -0400

fractgen (2.1.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 4.1.3.
    - Update debhelper dependency from >= 10 to >= 11.
  * Bump compat from 10 to 11.
  * Update copyright years.
  * Update Vcs-* fields (migrated to Git/Salsa).
  * Remove trailing whitespaces.
  * debian/examples: Install examples from src/examples.

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 23 Mar 2018 13:16:47 +0100

fractgen (2.1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 4.1.0.
    - Update debhelper dependency from >= 9 to >= 10.
  * Bump compat from 9 to 10.
  * Remove patches adding desktop file: Instead, add it to debian/.
  * Update copyright years.

 -- Hugo Lefeuvre <hle@debian.org>  Thu, 05 Oct 2017 22:28:33 +0200

fractgen (2.1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update dependencies to use Qt5 instead of Qt4.
  * debian/rules:
    - Force use of Qt5 via QT_SELECT.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 18 Dec 2016 13:56:54 +0100

fractgen (2.0.19-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.8.

 -- Hugo Lefeuvre <hle@debian.org>  Sat, 18 Jun 2016 17:25:43 +0200

fractgen (2.0.18-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.7.
    - Update the maintainer's e-mail address.
    - Update Vcs-Browser field to use an encrypted transport protocol.
  * debian/copyright:
    - Update copyright years.
    - Update the maintainer's e-mail address.
  * debian/rules:
    - Enable all hardening options to compile as a PIE and add the bindnow
      flag.
  * Remove debian/menu to make the package CTTE #741573 compliant.

 -- Hugo Lefeuvre <hle@debian.org>  Mon, 28 Mar 2016 12:09:13 +0200

fractgen (2.0.17-1) unstable; urgency=low

  * Initial release (Closes: #669893)

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Mon, 13 Jul 2015 12:25:02 +0200
